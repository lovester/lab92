const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const nanoid = require('nanoid');
const app = express();
const config = require('./config');
const auth = require('./middleware/auth');

const expressWs = require('express-ws')(app);

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const users = require('./app/users');


const port = 8000;

const activeConnections = {};

app.ws('/chat', (ws, req) => {
    console.log(req);
    const id = nanoid();
    console.log('client connected, id = ', id);
    activeConnections[id] = ws;

    Object.keys(activeConnections).forEach(connId => {
        const conn = activeConnections[connId];
        console.log(connId)
    });

    ws.on('message', msg => {
        const decodedMessage = JSON.parse(msg);
        console.log('client sent: ', decodedMessage);

        switch (decodedMessage.type) {
            case 'CREATE_MESSAGE':
                const message = JSON.stringify({
                    type: 'NEW_MESSAGE', message: {
                        user: decodedMessage.user,
                        text: decodedMessage.text,
                    }
                });

                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId];
                    conn.send(message);
                });
                break;
            default:
                console.log('Unknown message type:', decodedMessage.type);
        }
    });

    ws.on('close', msg => {
        console.log('client disconnected');
        delete activeConnections[id];
    })
});

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});