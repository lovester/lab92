const express = require('express');

const auth = require('../middleware/auth');

const User = require('../models/User');

const router = express.Router();

router.get('/online', async (req, res) => {
    const users = await User.find({status: 'online'});
    res.send(users);
});

router.get('/', async (req, res) => {
    const users = await User.find();
    res.send(users);
});

router.post('/', async (req, res) => {
    const user = new User({
        username: req.body.username,
        password: req.body.password
    });

    user.generateToken();

    try {
        await user.save();
        return res.send({message: 'User registered', user});
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(400).send({error: 'User does not exist'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Password incorrect'});
    }

    user.generateToken();
    user.status = 'online';
    await user.save();

    res.send({message: 'Login successful', user});
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Logged out'};

    if(!token) {
        return res.send(success);
    }

    const user = await User.findOne({token});

    if(!user) {
        return res.send(success);
    }

    user.generateToken();
    user.status = 'offline';
    await user.save();

    return res.send(success);
});

router.put('/', async (req, res) => {
    const token = req.get('Authorization');

    if (!token) {
        return res.status(401).send({error: 'Authorization headers not present'});
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.status(401).send({error: 'Token incorrect'});
    }

    user.password = req.body.password;

    await user.save();

    res.sendStatus(200);
});

module.exports = router;
