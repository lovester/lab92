import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";

class Chat extends Component {
    state = {
        messageText: ''
    };

    componentDidMount() {
        this.websocket = new WebSocket('ws://localhost:8000/chat');

        this.websocket.onmessage = event => {
            const decodedMessage = JSON.parse(event.data);
            if (decodedMessage.type === 'NEW_MESSAGE') {
                this.props.setMessage(decodedMessage.message);
            }
        };
    }

    changeInputHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    sendMessage = () => {
        const message = JSON.stringify({
            type: 'CREATE_MESSAGE',
            text: this.state.messageText,
            user: this.props.user.username
        });
        this.websocket.send(message);
        this.setState({messageText: ''});
    };

    render() {
        let chat = (
            <div>
                {this.props.messages.map((message, i) => (
                    <div key={i}>
                        <b>{message.user}</b>:
                        <span>{message.text}</span>
                    </div>
                ))}
                <div>
                    <input
                        type="text"
                        name="messageText"
                        value={this.state.messageText}
                        onChange={this.changeInputHandler}
                    />
                    <input type="button" value="Send" onClick={this.sendMessage} />
                </div>
            </div>
        );

        if (!this.props.user) {
            chat = (<div>please log in</div>);
        }

        return (
            <div className="App">
                {chat}
            </div>
        );

    }

}

const mapStateToProps = state => ({
    user: state.users.user,
    messages: state.messages.messages
});

const mapDispatchToProps = dispatch => ({
    setMessage: (message) => dispatch({type: 'SET_MESSAGE', message})
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);