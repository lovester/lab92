import React, {Component} from 'react';
import './UserList.css'
import connect from "react-redux/es/connect/connect";
import {getUsersOnline} from "../../store/actions/usersActions";

class UsersList extends Component {

    componentDidMount() {
        this.props.getUsersOnline();
    }

    render() {

        return (
            <div className="UsersList">
                {this.props.usersList ?
                    <ul>
                        Users online:
                        {this.props.usersList.map(user => {
                            return <li>{user.username}</li>
                        })}
                    </ul> : null
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    usersList: state.users.usersList
});

const mapDispatchToProps = dispatch => ({
    getUsersOnline: () => dispatch(getUsersOnline())
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);