import React, {Component, Fragment} from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";

import {logoutUser} from "./store/actions/usersActions";

import Routes from "./Routes";
import Layout from "./containers/Layout/Layout";

class App extends Component {

    render() {

        return (
            <Fragment>
                <NotificationContainer/>
                <Layout>
                    <Routes/>
                </Layout>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
