const initialState = {
    messages: []
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_MESSAGE':
            console.log(action.message);
            return{...state, messages: [...state.messages, action.message]};
        default:
            return state;
    }
};

export default usersReducer;