import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';
import axios from '../../axios-api';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const ADD_USER_TO_LIST = 'ADD_USER_TO_LIST';

export const LOGOUT_USER = 'LOGOUT_USER';

export const GET_USERS_ONLINE_SUCCESS = 'GET_USERS_ONLINE_SUCCESS';
export const GET_USERS_ONLINE_FAILURE = 'GET_USERS_ONLINE_FAILURE';

const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

const addUserToList = name => ({type: ADD_USER_TO_LIST, name});

const getUsersOnlineSuccess = users => ({type: GET_USERS_ONLINE_SUCCESS, users});
const getUsersOnlineFailure = error => ({type: GET_USERS_ONLINE_FAILURE, error});

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.delete('/users/sessions', config).then(
            () => {
                dispatch({type: LOGOUT_USER});
                NotificationManager.success('Logged out!');
                dispatch(getUsersOnline());
            },
            error => {
                NotificationManager.error('Could not logout!');
            }
        )
    }
};

export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess(response.data.user));
                NotificationManager.success('Registered successfully!');
                dispatch(push('/'));
            },
            error => {
                if (error.response) {
                    dispatch(registerUserFailure(error.response.data));
                } else {
                    dispatch(registerUserFailure({global: 'No connection'}));
                }
            }
        )
    }
};

export const loginUser = userData => {
    return (dispatch, getState) => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Logged in successfully!');
                dispatch(addUserToList(response.data.user.username));
                dispatch(getUsersOnline());
                dispatch(push('/'));
            },
            error => {
                if (error.response) {
                    dispatch(loginUserFailure(error.response.data));
                } else {
                    dispatch(loginUserFailure({global: 'No connection'}));
                }
            }
        )
    }
};

export const getUsersOnline = () => {
    return (dispatch) => {
        return axios.get('/users/online').then(
            response => {
                dispatch(getUsersOnlineSuccess(response.data));
            },
            error => {
                if (error.response) {
                    dispatch(getUsersOnlineFailure(error.response.data));
                } else {
                    dispatch(getUsersOnlineFailure({global: 'No connection'}));
                }
            }
        )
    }
};
